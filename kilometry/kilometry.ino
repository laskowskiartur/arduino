#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 8);

int magnet = 2;
int LED = 13;
int tmp = 0;
int signal = 1;
int state = 1;
double ray = 0.4064;  // in fact 40,64 cm
double pi = 3.1415;
double circuit = 2 * pi * ray;

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(magnet, INPUT);
  lcd.begin(16, 2); 
  lcd.print("hello, world!");
}

void loop() {
  lcd.setCursor(0, 1);
  if(digitalRead(magnet)==HIGH){
    if(state==0){
      state = 1;
    }
  }
  if(digitalRead(magnet)==LOW){
    if(state==1){
      state = 0;
      tmp++;
    }
  }
  lcd.print(tmp * circuit / 1000 );
  lcd.print(" km/h");
}
