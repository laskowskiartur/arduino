#include <LiquidCrystal.h>

// binding with digital inputs on Funduin
LiquidCrystal lcd(12, 11, 5, 4, 3, 8);
int magnet = 2;
int LED = 13;
int pushButton = 7;

int counter = 0;            // counting number of circle cycles
int reedState = 1;          // state of reed (1 - HIGH, 0 - LOW)
int buttonState = 0;        // current state of button
int prevButtonState = 0;    // previous state of button
int displayMode = 0;        // mode (distance, time, current speed, overall speed) 
int prevMillis = 0;         // milisecond from begining of program till previous magnes signal
int actMillis = 0;          // milisecond from begining of program till last magnes signal
double hours = 0;           // hours from beginning of program till now
double ray = 0.3302;        // ray of wheel
double pi = 3.1415;
double circuit = 2 * pi * ray;
double distance = 0;        // actual distance completed from begining of program

void setup() {
  pinMode(pushButton, INPUT);      // for signals from button
  pinMode(magnet, INPUT);          // for signals from reed
  lcd.begin(16, 2);                // starting display
  //////////// HELLO MESSAGE /////////////
  lcd.print("Counter by Artur");
  lcd.setCursor(0, 1);
  lcd.print("Laskowski 109680");
  delay(4000);
  //////////// HELLO MESSAGE //////////////
  lcd.clear();
}

void loop() {
  // waiting for state change on reed
  if(digitalRead(magnet)==HIGH){
    if(reedState==0){
      reedState = 1;
    }
  }
  if(digitalRead(magnet)==LOW){
    if(reedState==1){
      reedState = 0;
      counter++;
      prevMillis = actMillis;
      actMillis = millis();
    }
  }
  
  // --------------------------------------------------
  
  // waiting for user to click the button
  buttonState = digitalRead(pushButton);  
  if( buttonState == 1 && prevButtonState == 0 ){
    displayMode += 1;
    displayMode %= 4;
  }
  prevButtonState = buttonState;
  
  // ---------------------------------------------------
  
  // choosing correct mode and displaying proper content
  hours = millis() / 3600000.0;
  distance = counter * circuit / 1000.0;
  if(displayMode == 0){
    lcd.setCursor(0, 0);
    lcd.print("Distance:       ");
    lcd.setCursor(0, 1);
    lcd.print( distance );
    lcd.print(" km     ");
  }
  else if(displayMode == 1){
    lcd.setCursor(0, 0);
    lcd.print("Time:           ");
    lcd.setCursor(0, 1);
    lcd.print( hours );
    lcd.print(" hours      ");
  }
  else if(displayMode == 2){
    lcd.setCursor(0, 0);
    lcd.print("Overall speed:  ");
    lcd.setCursor(0, 1);
    if( hours > 0 ){
      lcd.print( distance / hours );
    } else {
      lcd.print( "0.00" );
    }
    lcd.print(" km/h     ");
  }
  else if(displayMode == 3){
    int interval = actMillis - prevMillis;
    lcd.setCursor(0, 0);
    lcd.print("Current speed:  ");
    lcd.setCursor(0, 1);
    if( interval > 0 ){
      lcd.print( circuit * 3600 / interval );
    } else {
      lcd.print( "0.00" );
    }
    lcd.print(" km/h     ");
  }
}
