#include <LiquidCrystal.h>

//Inicjalizacja połączeń
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() { 
// Wybór rodzaju wyświetlacza  - 16x2
lcd.begin(16, 2); 
//Przesłanie do wyświetlania łańcucha znaków hello, world!
lcd.print("hello, world!");
}
 
void loop(){ 
//Przejście kursora do pierwszej kolumny drugiego wiersza
lcd.setCursor(0, 1);
//Odczyt oraz wyświetlenie czasu jaki upłynął od ostatniego resetu w sekundach
lcd.print(millis()/1000);
}
