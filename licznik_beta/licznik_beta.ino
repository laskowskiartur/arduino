#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 8);

int magnet = 2;
int LED = 13;
int pushButton = 7;
int tmp = 0;
int signal = 1;
int state = 1;
int buttonState;
int prevButtonState = 0;
int mode = 0;
double ray = 0.4064;  // in fact 40,64 cm
double pi = 3.1415;
double circuit = 2 * pi * ray;

void setup() {
  pinMode(pushButton, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(magnet, INPUT);
  lcd.begin(16, 2); 
  lcd.print("hello, world!");
}

void loop() {
  lcd.setCursor(0, 1);
  if(digitalRead(magnet)==HIGH){
    if(state==0){
      state = 1;
    }
  }
  if(digitalRead(magnet)==LOW){
    if(state==1){
      state = 0;
      tmp++;
    }
  }
  buttonState = digitalRead(pushButton);  
  if( buttonState == 1 && prevButtonState == 0 ){
    mode += 1;
    mode %= 3;
  }
  prevButtonState = buttonState;
  
  if(mode == 0){
    lcd.print(tmp * circuit / 1000 );
    lcd.print(" km     ");
  }
  if(mode == 1){
    double hours = millis()/3600000.0;
    lcd.print(hours);
    lcd.print(" hours    ");
  }
  if(mode == 2){
    double quickness = tmp * circuit / 1000;
    quickness /= millis()/3600000.0;
    lcd.print(quickness);
    lcd.print(" km/h    ");
  }
}
