int magnet = 2;
int LED = 13;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
  pinMode(magnet, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(magnet)==HIGH){
    digitalWrite(LED, HIGH);
  }
  if(digitalRead(magnet)==LOW){
    digitalWrite(LED, LOW);
  }
}
